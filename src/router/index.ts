import { createRouter, createWebHistory } from "vue-router";
import ProjectsView from "../views/ProjectsView.vue";
import ContactsView from "../views/ContactsView.vue";
import HelpView from "../views/HelpView.vue";
import ArchiveView from "../views/ArchiveView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/projects",
      name: "project",
      component: ProjectsView,
    },
    {
      path: "/archive",
      name: "archive",
      component: ArchiveView,
    },
    {
      path: "/contacts",
      name: "contacts",
      component: ContactsView,
    },
    {
      path: "/help",
      name: "help",
      component: HelpView,
    },
  ],
});

export default router;
